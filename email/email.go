package email

import (
	"database/sql"
	"gopkg.in/gomail.v2"
	"io"
)

func SendEmailByDB(db *sql.DB, message *Message) error {
	var err error
	if err = GetSmtp(db, message); err != nil {
		return err
	}

	return SendEmail(message)
}

func SendEmail(message *Message) error {
	m := gomail.NewMessage()
	m.SetHeader("From", message.From)
	m.SetHeader("To", message.To...)
	m.SetHeader("Subject", message.Subject)
	m.SetBody(message.BodyContentType, message.Body)

	if message.Attach != nil {
		m.Attach(message.AttachName, gomail.SetCopyFunc(func(w io.Writer) error {
			_, err := w.Write(message.Attach)
			return err
		}))
	}

	// Send email.
	d := gomail.NewDialer(message.Server, message.Port, message.User, message.Password)
	if err := d.DialAndSend(m); err != nil {
		return err
	}

	return nil
}

func GetSmtp(db *sql.DB, message *Message) error {
	rows, err := db.Query("SELECT SMTP_SERVER, SMTP_PORT, SMTP_FROM, SMTP_USER, SMTP_PASSWORD " +
		"FROM preferences")
	if err != nil {
		return err
	}
	defer rows.Close()

	if rows.Next() {
		if err = rows.Scan(&message.Server, &message.Port, &message.From, &message.User, &message.Password); err != nil {
			return err
		}
	}

	return nil
}
