package email

type Message struct {
	Server          string
	Port            int
	From            string
	User            string
	Password        string
	To              []string
	Subject         string
	BodyContentType string
	Body            string
	AttachName      string
	Attach          []byte
}
