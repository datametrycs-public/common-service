package misc

import (
	"bytes"
	"strconv"
	"strings"
	"time"
)

var uctLocation *time.Location

// init()
func init() {
	uctLocation, _ = time.LoadLocation("UTC")
}

// Get 'UTC' loction
func GetUtcLocation() *time.Location {
	return uctLocation
}

// Move date from a certain time zone to UTC.
func ParseDateInUtc(date, layout string, location *time.Location) (string, error) {
	d, err := time.ParseInLocation(layout, date, location)
	if err != nil {
		return "", err
	}
	return d.In(GetUtcLocation()).Format(layout), nil
}

// String NVL.
func StringNVL(name string, valueForEmpty string) string {
	if name != "" {
		return name
	}
	return valueForEmpty
}

// Compare two values.
func CompareTo(a interface{}, b interface{}) int {

	switch v := a.(type) {

	case string:
		{
			if v == b.(string) {
				return 0
			}
			if v > b.(string) {
				return 1
			}
			return -1
		}
	case float32:
		{
			if v == b.(float32) {
				return 0
			}
			if v > b.(float32) {
				return 1
			}
			return -1
		}
	case uint64:
		{
			if v == b.(uint64) {
				return 0
			}
			if v > b.(uint64) {
				return 1
			}
			return -1
		}
	case uint32:
		{
			if v == b.(uint32) {
				return 0
			}
			if v > b.(uint32) {
				return 1
			}
			return -1
		}
	case uint16:
		{
			if v == b.(uint16) {
				return 0
			}
			if v > b.(uint16) {
				return 1
			}
			return -1
		}
	case uint8:
		{
			if v == b.(uint8) {
				return 0
			}
			if v > b.(uint8) {
				return 1
			}
			return -1
		}
	case int:
		{
			if v == b.(int) {
				return 0
			}
			if v > b.(int) {
				return 1
			}
			return -1
		}
	case int64:
		{
			if v == b.(int64) {
				return 0
			}
			if v > b.(int64) {
				return 1
			}
			return -1
		}
	case int32:
		{
			if v == b.(int32) {
				return 0
			}
			if v > b.(int32) {
				return 1
			}
			return -1
		}
	case int16:
		{
			if v == b.(int16) {
				return 0
			}
			if v > b.(int16) {
				return 1
			}
			return -1
		}
	case int8:
		{
			if v == b.(int8) {
				return 0
			}
			if v > b.(int8) {
				return 1
			}
			return -1
		}
	case float64:
		{
			if v == b.(float64) {
				return 0
			}
			if v > b.(float64) {
				return 1
			}
			return -1
		}
	case time.Time:
		{
			if v.Equal(b.(time.Time)) {
				return 0
			}
			if v.After(b.(time.Time)) {
				return 1
			}
			return -1
		}
	}

	return 0
}

// Return correct value as uint64.
func GetValueAsUint64(a interface{}) uint64 {

	switch a.(type) {
	case uint64:
		{
			return a.(uint64)
		}
	case uint32:
		{
			return uint64(a.(uint32))
		}
	case uint16:
		{
			return uint64(a.(uint16))
		}
	case uint8:
		{
			return uint64(a.(uint8))
		}
	case int64:
		{
			return uint64(a.(int64))
		}
	case int32:
		{
			return uint64(a.(int32))
		}
	case int16:
		{
			return uint64(a.(int16))
		}
	case int8:
		{
			return uint64(a.(int8))
		}
	case float32:
		{
			return uint64(a.(float32))
		}
	case float64:
		{
			return uint64(a.(float64))
		}
	}

	return 0
}

// Return correct value as float32.
func GetValueAsFloat32(a interface{}) float32 {

	switch a.(type) {
	case float32:
		{
			return a.(float32)
		}
	case float64:
		{
			return float32(a.(float64))
		}
	case uint64:
		{
			return float32(a.(uint64))
		}
	case uint32:
		{
			return float32(a.(uint32))
		}
	case uint16:
		{
			return float32(a.(uint16))
		}
	case uint8:
		{
			return float32(a.(uint8))
		}
	case int64:
		{
			return float32(uint64(a.(int64)))
		}
	case int32:
		{
			return float32(uint64(a.(int32)))
		}
	case int16:
		{
			return float32(uint64(a.(int16)))
		}
	case int8:
		{
			return float32(uint64(a.(int8)))
		}
	}

	return 0
}

// Get the difference between two instants in seconds.
func GetDuration(start time.Time) float64 {
	return time.Now().Sub(start).Seconds()
}

// Return the RETENTION_PERIOD always in days. Remember the 'rp' parameter comes:
// - in Days if it's negative (between -1 and -25)
// - in Months if it's positive (between 1 and 60)
// - and the unlimited value of 600
func GetRetentionPeriod(rp int) int {
	if rp < 0 {
		return -rp
	}

	return rp * 31
}

// This function returns the progress of a thermometer.
func GetProgress(info [][]interface{}) string {
	var s bytes.Buffer
	s.WriteString("[")

	// Remember each row contains: [ { queryId (string), readRows (uint64), totalRows (uint64), elapsed (float64), error } ]
	for _, data := range info {
		s.WriteString("{\"id\":\"")
		s.WriteString(data[0].(string))
		s.WriteString("\",\"rr\":")
		s.WriteString(strconv.FormatUint(data[1].(uint64), 10))
		s.WriteString(",\"tr\":")
		s.WriteString(strconv.FormatUint(data[2].(uint64), 10))
		s.WriteString(",\"ts\":")
		s.WriteString(strconv.FormatFloat(data[3].(float64), 'f', -1, 64))
		s.WriteString(",\"err\":\"")
		s.WriteString(data[4].(string))
		s.WriteString("\"},")
	}

	s.WriteString("]")
	jsonDoc := s.String()

	if strings.HasSuffix(jsonDoc, ",]") {
		return strings.Replace(jsonDoc, ",]", "]", 1)
	}

	return jsonDoc
}
