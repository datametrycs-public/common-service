package misc

func Round(v float64, decimals int) float64 {

	var pow float64 = 1

	for i := 0; i < decimals; i++ {
		pow *= 10
	}

	if v < 0 {
		return float64(int((v*pow)-0.5)) / pow
	}
	return float64(int((v*pow)+0.5)) / pow

}
