package misc

import (
	"sort"
)

// This function sorts and gets the arrays for charts and reports. For 'ONE_GROUP_NOTIME', 'TWO_GROUPS_NOTIME' and to sort the second measure of 'TWO_GROUPS_FIRST_TIME'.
func SortAndGet(arr [][]interface{}, results int, order string, indexToSort int) [][]interface{} {

	if len(arr) > 1 {

		// Sort
		sort.Slice(arr, func(i, j int) bool {
			if order == "D" {
				return CompareTo(arr[i][indexToSort], arr[j][indexToSort]) == 1
			}

			return CompareTo(arr[i][indexToSort], arr[j][indexToSort]) == -1
		})

		// And limit the results.
		if results > 0 && len(arr) > results {
			return arr[0:results]
		}

	}

	return arr
}
