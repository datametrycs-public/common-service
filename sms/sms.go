package sms

import (
	commonHttp "bitbucket.org/datametrycs-public/common-service/http"
	"errors"
	"io"
	"net/http"
	"strings"
)

const (
	kConnectTimeout  = 2000
	kResponseTimeout = 10000
	kRequest         = `{
					"communication_type_key": "SMS",
					"sender_key": "S_SMSUP_DATAMETRYCS",
					"template_key": "T_S_MESSAGE",
					"receiver_name": "{0}",
					"receiver_to": "{1}",
					"external_user_id": "1",
					"queue": false,
					"params": {
						"MESSAGE": "[{2}] {3}"
				}}`
)

var objHttp = commonHttp.CreateHttpClient(kConnectTimeout, kResponseTimeout)

// Send : This function sends a json post request to the 'sms' service if the interval from the last SMS sent has been passed
func Send(url, apiKey, receiverName, phone, messageTitle, messageBody string) error {
	var req *http.Request
	var resp *http.Response
	var err error

	req, err = http.NewRequest(http.MethodPost, url, strings.NewReader(strings.Replace(strings.Replace(strings.Replace(strings.Replace(kRequest, "{0}", receiverName, 1), "{1}", phone, 1), "{2}", messageTitle, 1), "{3}", messageBody, 1)))
	req.Header.Set("Content-Type", commonHttp.APPLICATION_JSON)
	req.Header.Set("X-API-KEY", apiKey)

	resp, err = objHttp.Do(req)
	if err != nil {
		return err
	}

	// Read the response body.
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	// If code = 200, return 'OK'
	if resp.StatusCode == http.StatusOK {
		return nil
	}

	// If we had an error, return it.
	if err != nil {
		return err
	}

	// If we had not an error, return the body.
	return errors.New(string(body))
}
