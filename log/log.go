package log

import (
	"github.com/sirupsen/logrus"
	"github.com/x-cray/logrus-prefixed-formatter"
)

func Create() *logrus.Logger {

	log := logrus.New()

	// Set the log level.
	log.SetLevel(logrus.InfoLevel)

	// Create a message formatter and set ti to the log.
	Formatter := new(prefixed.TextFormatter)
	Formatter.TimestampFormat = "2006-01-02 15:04:05"
	Formatter.FullTimestamp = true
	Formatter.ForceFormatting = true
	log.SetFormatter(Formatter)

	return log
}
