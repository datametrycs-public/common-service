package model

const (
	// Query Type
	QUERY_TYPE  = "Q"
	REPORT_TYPE = "R"

	// Sector code / Subsector codes.
	TOURISM_SECTOR         = "TOU"
	HOTELS_SUBSECTOR       = "HO"
	SUPPLIERS_SUBSECTOR    = "SU"
	ACTIVITIES_SUBSECTOR   = "AG"
	FLIGHT_STATS_SUBSECTOR = "FSTATS"
	APM_SUBSECTOR          = "APM"

	// Number Formats
	INTEGER_FORMAT    = "#,###."
	FLOAT_FORMAT_2DEC = INTEGER_FORMAT + "##"
	FLOAT_FORMAT_3DEC = INTEGER_FORMAT + "###"
	FLOAT_FORMAT_4DEC = INTEGER_FORMAT + "####"

	// Date Formats
	DATE_LAYOUT      = "2006-01-02"
	DATE_HOUR_LAYOUT = DATE_LAYOUT + " 15:04:05"

	// Hours
	HOURS_IN_DAY = 24
	MINIMUM_HOUR = "00:00:00"
	MAXIMUM_HOUR = "23:59:59"

	// Chart types
	NO_GROUP              = 1
	ONE_GROUP_NOTIME      = 2 // [ CLIENT_ID, client_name, avails, bookings, l2b ]
	ONE_GROUP_TIME        = 3 // [ REQUEST_DATE, request_date, avails, bookings, l2b ]
	TWO_GROUPS_NOTIME     = 4 // [ CLIENT_ID, HOTEL_ID, client_name, hotel_name, avails, bookings, l2b ]
	TWO_GROUPS_FIRST_TIME = 6 // [ REQUEST_DATE, CLIENT_ID, '', client_name, avails, bookings, l2b ]

	// SQL Pattern for functions with TimeZone.
	TIMEZONE_PATTERN = "*TZ*"

	// Time measure functions.
	DATE_FUNC_TODATE               = "toDate"
	DATE_FUNC_YYYYMM               = "toYYYYMM"
	DATE_FUNC_MONTH                = "toMonth"
	DATE_FUNC_DAY_OF_WEEK          = "toDayOfWeek"
	DATE_FUNC_TO_START_OF_INTERVAL = "toStartOfInterval"
	TIME_WINDOW_PREFIX             = "TW"

	// Time descriptions for 'toStartOfInterval'
	SECOND_DESCRIPTION = "SECOND"
	MINUTE_DESCRIPTION = "MINUTE"
	HOUR_DESCRIPTION   = "HOUR"

	// Measure types
	TIME_MEASURE     = "T"
	LOCATION_MEASURE = "L"
	OTHER_MEASURE    = ""

	REQUEST_MEASURE  = "RQ"
	RESPONSE_MEASURE = "RS"

	// Order
	ASCENDENT_ORDER  = "A"
	DESCENDENT_ORDER = "D"
	CATEGORY_ORDER   = "C"

	// max results in case of time-no_time
	MAX_RESULTS = 100

	// Name of the 'results' table.
	RESULTS_TABLE_NAME = "RESULTS"
	RESULTS_TABLE_TTL  = 5
)

var (
	ARRAY_GROUP_FUNCTIONS = []string{"COUNT", "SUM", "MAX", "MIN", "AVERAGE"}
)
