package model

import (
	"bitbucket.org/datametrycs-public/common-service/misc"
	"bytes"
	"log"
	"math"
	"strconv"
	"strings"
	"time"
)

// Query object.
type DMQuery struct {

	// Admin data.
	Id        uint64  `json:"id"`
	Sector    DMValue `json:"sector"`
	Subsector DMValue `json:"subsector"`
	Company   DMValue `json:"company"`
	Api       DMApi   `json:"api"`
	User      DMUser  `json:"user"`
	Kpi       DMValue `json:"kpi"`

	// SQL parts.
	SqlSelect string       `json:"sqlSelect"`
	SqlFrom   string       `json:"sqlFrom"`
	Filter    []*DMSQLElem `json:"filter"`
	OwnFilter string       `json:"ownFilter"`
	Group     []*DMSQLElem `json:"group"`
	SqlOrder  string       `json:"sqlOrder"`
	Limit     string

	// View details, dates and order
	Type    string `json:"type"`    // (Q)uery or (R)eport
	Name    string `json:"name"`    // View name
	From    string `json:"from"`    // Always 'yyyy-MM-dd HH:mm:ss' format (it's the 'from' field from the datametrycs view search form, date + hour)
	To      string `json:"to"`      // Always 'yyyy-MM-dd HH:mm:ss' format (it's the 'to' field from the datametrycs view search form, date + hour)
	Results int    `json:"results"` // Number of results to show for the first group-by measure.
	Order   string `json:"order"`   // Result order for the first group-by measure, 'A' or 'D'

	// Benchmark
	Benchmark *DMSQLElem `json:"benchmark"` // Benchmark (nil if it does not exist).

	// Comparatives
	Compare *DMCompare `json:"compare"` // Comparative (nil if it does not exist).

	// Other details.
	ChartType int               `json:"chartType"` // Chart type, from 1 to 6
	Params    map[string]string `json:"params"`    // Here we can set parameters depending on each subsector. For example, for tourism -> hotels we can have up to 2 different parameters:
	// - btcDate string											- just for 'book to cancel', the date to show the list of cancels and bookings.
	// - btcId uint32											- just for 'book to cancel', the concrete 'id' to show the list of cancels and bookings. Only when we have a 'group by'
	//
	//												  And for charts with 1 group by NO-TIME, we can skip from 'bar' to 'pie' and from 'pie' to bar, so the informations regarding this is sent in a new param call 'barOrPie' with values 'B'/'P'
	// - barOrPie

	// Report subtotals (for the first group by) and totals (for the end of the report)
	SubTotals string // Report subtotals: values can be 'COUNT,SUM,MAX,MIN,AVG'
	Totals    string // Report totals: values can be 'COUNT,SUM,MAX,MIN,AVG'

	// Results returned.
	ChartResult interface{} `json:"result"` // The query result to paint
	Rows        uint64
	Took        float64
	Error       string

	// These struct is used when executing reports in report-service, we need to store 3 values to be obtained after the execution of the report (see 'ReportService.executor.GetReportResults()')
	ExecuteReportDetails *DMExecuteReportDetails
}

// 'Value' struct.
type DMSQLElem struct {
	Id           int        `json:"id"`   // Measure ID
	Code         string     `json:"code"` // Measure CODE (same than mapping code)
	Description  string     `json:"description"`
	Type         string     `json:"type"`        // Measure Type: (T)ime, (L)ocation or ""
	MappingCode  string     `json:"mappingCode"` // Measure mapping code, "" if no mapping is associated to this measure.
	RqRs         string     `json:"rqRs"`        // Request measure ('RQ') or response measure ('RS')
	DbColumnName string     `json:"dbColumnName"`
	Values       []*DMValue `json:"values"`      // List of values in case of 'IN'
	ViewOwnData  bool       `json:"viewOwnData"` // Only for benchmarks, if the user selected in the benchmark can view all data or only his own data.

	TwMeasureCode  string `json:"twMeasureCode"`  // Only for time window filter, the code of the real measure to apply
	TwMeasureValue string `json:"twMeasureValue"` // Only for time window filter, the value to apply ('T' for today for example).

	TmpTableName string // This is used just when we have a large 'IN' clause and we need to use a temporary table on this filter
}

type DMCompare struct {
	From string `json:"from"` // Always 'yyyy-MM-dd' format
	To   string `json:"to"`   // Always 'yyyy-MM-dd' format
}

type DMExecuteReportDetails struct {
	Name    string
	Format  string
	Emails  []string
	From    string
	To      string
	ByEmail bool
}

// 'Api' struct
type DMApi struct {
	Id          uint32   `json:"id"`
	Code        string   `json:"code"`
	Description string   `json:"description"`
	Properties  []string `json:"properties"`
}

// 'Value' struct.
type DMValue struct {
	Id             uint32 `json:"id"`
	Code           string `json:"code"`
	Description    string `json:"description"`
	AdditionalInfo string `json:"additionalInfo"`
}

// 'User' struct.
type DMUser struct {
	Id       int            `json:"id"`
	Name     string         `json:"name"`
	Email    string         `json:"email"`
	TimeZone string         `json:"timezone"` // The user time zone.
	Location *time.Location `json:"-"`        // This attribute is set on the 'query/controller/Handle' method.
}

// Returned result
type DMResult struct {
	Data interface{} // For example, for kpi = 'L2B' the attribute 'Data' will be a '*hotelmodel.DML2bRps'
	Rows uint64
	Took float64
	Err  error
}

// 'Totals' struct
type DMTotals struct {
	Count              uint64
	Sum, Max, Min, Avg float32
}

// ---------------------------------------------------------------------------------------------------------------------
// Object that contains a measure --------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
type MeasureData struct {
	Id              int
	Code            string
	Name            string
	DbName          string
	MappingCode     string
	SubsectorId     int
	RqRs            string
	Type            string // (L)ocation (if 'OrderByLocation > 0'), (T)ime (if 'OrderByTime > 0'), (O)ther ('OrderByLocation == 0 && OrderByTime = 0')
	OrderByTime     int
	OrderByLocation int
	ShowInReport    int
}

var aggrFunctions = []string{"count", "sum", "uniq", "avg", "min", "max", "any", "arg", "group", "quantile", "median", "var", "std", "covar", "corr", "sequence"}

// ------------- METHODS -----------------------------------------------------------------------------------------------

// Set the 'count' (it always comes the first)
func (query *DMQuery) SetDefaultCount() {
	query.SetSelect("COUNT() as cTotal")
}

// Set the 'count' (it always comes the first)
func (query *DMQuery) SetCount(count string) {
	query.SetSelect(count)
}

// This function sets part of the 'select'.
func (query *DMQuery) SetSelect(sqlSelect string) {
	query.SqlSelect = sqlSelect
}

// This function gets part of the 'select'.
func (query *DMQuery) GetSelect() string {
	return query.SqlSelect
}

// This function adds an additional 'select' for this query.
func (query *DMQuery) AddSelect(sqlSelect string) {
	if query.SqlSelect == "" {
		query.SqlSelect = sqlSelect
	} else {
		query.SqlSelect += ", " + sqlSelect
	}
}

// This function sets part of the 'from'.
func (query *DMQuery) SetFrom(sqlFrom string) {
	query.SqlFrom = sqlFrom
}

// This function gets part of the 'from'.
func (query *DMQuery) GetFrom() string {
	return query.SqlFrom
}

// This function returns if the select contains just an aggregate function (no fields, just "SELECT COUNT()" ...)
func (query *DMQuery) IsOnlyAggregateSelect() bool {

	aggr := strings.ToLower(query.SqlSelect)
	for _, s := range aggrFunctions {
		if strings.HasPrefix(aggr, s) {
			return true
		}
	}

	return false
}

// This function sets an own filter.
func (query *DMQuery) SetOwnFilter(filter string) {
	query.OwnFilter = filter
}

// This function adds another own filter to the current query filter.
func (query *DMQuery) AddOwnFilter(filter string) {

	if query.OwnFilter == "" {
		query.OwnFilter = filter
	} else {
		query.OwnFilter += " AND " + filter
	}

}

// This function gets the 'order by' for this query.
func (query *DMQuery) GetOrderBy() string {
	return query.SqlOrder
}

// This function sets and 'order by' for this query
func (query *DMQuery) SetOrderBy(orderBy string, direction string) {

	if orderBy != "" {

		query.SqlOrder = "ORDER BY " + orderBy
		if direction == DESCENDENT_ORDER {
			query.SqlOrder += " DESC"
		}

	} else {
		query.SqlOrder = ""
	}
}

// This function adds an additional 'order by' for this query.
func (query *DMQuery) AddOrderBy(orderBy string, direction string) {

	if query.SqlOrder == "" {
		query.SqlOrder = "ORDER BY "
	} else {
		query.SqlOrder += ", "
	}

	query.SqlOrder += orderBy
	if direction == DESCENDENT_ORDER {
		query.SqlOrder += " DESC"
	}
}

// This function builds the 'standard' order by for a single query. By single query we mean only one execution, not like 'l2b' or 'rps' that need several executions to be computed.
// It sets the 'order by' to get the slice already in the right order. The query is sorted IF AND OLY IF we have a group by clause. So:
// | GROUP BY CLIENT_ID 				| (ONE_GROUP_NOTIME = 2) 		--> result must be ordered by value ( [ [ C1, 5], [ C2, 4 ], [ C3, 12 ] ]  -->  [ [ C3, 12], [ C1, 5 ], [ C2, 4 ] ] )
// | GROUP BY REQUEST_DATE 				| (ONE_GROUP_TIME = 3) 			--> we must sort only by date.
// | GROUP BY REQUEST_DATE, CLIENT_ID 	| (TWO_GROUPS_FIRST_TIME = 6) 	--> we must sort by date.
// Remember the field containing the value of the aggregated function must have an alias named 'cTotal'.
func (query *DMQuery) SetStandardOrder() {

	if query.Group != nil {

		if len(query.Group) == 1 {

			// We add the 'WITH FILL' just in 1-group by with time measure and IF AND ONLY IF the time measure is not 'toYYYYMM'
			if query.Group[0].Type == TIME_MEASURE {
				query.SqlOrder = "ORDER BY " + query.Group[0].DbColumnName
				if !strings.HasPrefix(query.Group[0].DbColumnName, DATE_FUNC_YYYYMM) {
					query.SqlOrder += " WITH FILL" + query.addFillStep()
				}
			} else {
				query.SqlOrder = "ORDER BY cTotal "
				if query.Order == DESCENDENT_ORDER {
					query.SqlOrder += "DESC"
				}
			}

		} else {
			query.SqlOrder = "ORDER BY " + query.Group[0].DbColumnName
		}
	}
}

// This function adds the 'STEP' clause to an 'ORDER BY' if 'toStartOfInterval' is being used.
// toStartOfInterval(REQUEST_DATE, INTERVAL 10 MINUTE) --> search by "INTERVAL ", get the text '10 MINUTE' and split by ' ' --> [ '10', 'MINUTE' ]
func (query *DMQuery) addFillStep() string {

	interval := ""

	if strings.HasPrefix(query.Group[0].DbColumnName, "toStartOfInterval") {
		arr := strings.Split(query.Group[0].DbColumnName, "INTERVAL ")
		arr = strings.Split(arr[1], " ") // --> arr[0] = '10', arr[1] = 'MINUTE)'
		arr[0] = strings.TrimSpace(arr[0])
		arr[1] = strings.TrimSpace(arr[1])

		interval += " STEP "
		if strings.HasPrefix(arr[1], SECOND_DESCRIPTION) {
			interval += arr[0]
		} else if strings.HasPrefix(arr[1], MINUTE_DESCRIPTION) {
			interval += arr[0] + " * 60"
		} else if strings.HasPrefix(arr[1], HOUR_DESCRIPTION) {
			interval += arr[0] + " * 60 * 60"
		}
	}

	return interval
}

// This function sets a filter with the array of 'DMSQLElem'.
func (query *DMQuery) SetFilterElement(elems []*DMSQLElem) {
	query.Filter = make([]*DMSQLElem, len(elems))
	for i, elem := range elems {
		query.Filter[i] = elem
	}
}

// This function adds a component to the 'filter by' clause.
func (query *DMQuery) AddFilterElement(elem *DMSQLElem) {
	if query.Filter == nil {
		query.Filter = make([]*DMSQLElem, 0, 1)
	}
	query.Filter = append(query.Filter, elem)
}

// This function removes a component from the 'filter by' clause and returns it.
func (query *DMQuery) RemoveFilterElement(code ...string) []*DMSQLElem {

	var result []*DMSQLElem

	if query.Filter != nil {

		for _, v := range code {

			for i, f := range query.Filter {

				if f.Code == v {

					query.Filter = append(query.Filter[:i], query.Filter[i+1:]...)
					if result == nil {
						result = make([]*DMSQLElem, 0, 1)
					}
					result = append(result, f)

					break
				}
			}
		}
	}

	return result
}

// This function checks if this element code is included in the filter.
func (query *DMQuery) GetFilterElement(code string) *DMSQLElem {

	if query.Filter != nil {

		for _, f := range query.Filter {
			if f.Code == code {
				return f
			}
		}
	}

	return nil
}

// This function adds a component to the 'group by' clause.
func (query *DMQuery) AddGroupByElement(elem *DMSQLElem) {
	if query.Group == nil {
		query.Group = make([]*DMSQLElem, 0, 1)
	}
	query.Group = append(query.Group, elem)
}

// This function removes a component from the 'group by' clause, and thus, from the 'select'. If the 'code' parameter is the empty string, the complete 'group by' is removed.
func (query *DMQuery) RemoveGroupByElement(code ...string) []*DMSQLElem {

	var result []*DMSQLElem

	if query.Group != nil {

		for _, v := range code {

			for i, g := range query.Group {

				if v == "" || g.Code == v {

					query.Group = append(query.Group[:i], query.Group[i+1:]...)
					query.SqlSelect = strings.Replace(query.SqlSelect, g.DbColumnName, "", 1)
					query.SqlSelect = strings.TrimSuffix(query.SqlSelect, ",")

					if result == nil {
						result = make([]*DMSQLElem, 0, 1)
					}
					result = append(result, g)

					break
				}
			}
		}

		if len(query.Group) == 0 {
			query.SqlSelect = ""
			query.Group = nil
		}
	}

	return result
}

// This function checks if this element code is included in the group-by.
func (query *DMQuery) GetGroupByElement(code string) *DMSQLElem {

	if query.Group != nil {

		for _, g := range query.Group {
			if g.Code == code {
				return g
			}
		}
	}

	return nil
}

// This function returns the sql limit
func (query *DMQuery) GetLimit() string {
	return query.Limit
}

// This function sets the sql limit and returns it.
func (query *DMQuery) SetDefaultLimit() string {
	query.SetLimit(query.Results)
	return query.GetLimit()
}

// This function sets the sql limit and returns it.
func (query *DMQuery) SetLimit(limit int) string {

	// We apply the 'limit' clause only in 1-groupBy --> We only have one element in 'group by' --> apply the limit to the groupBy[0] if and only if it has a mapping associated and 'Results' > 0.
	if len(query.Group) == 1 && query.Group[0].MappingCode != "" && limit > 0 {
		query.Limit = "LIMIT " + strconv.Itoa(limit)
	} else {
		query.Limit = ""
	}

	return query.GetLimit()
}

// This function builds a sql statement. IMPORTANT !!!! If we have date/datetime functions, replace the pattern '*TZ*' by the user timezone.
func (query *DMQuery) BuildQuery(tableName, filter string, params []interface{}) string {

	var sqlSelect, sqlFrom, groupBy, description string

	// Is there a group by clause?
	if query.Group != nil {

		groupBy = query.GetGroupBy()

		for _, group := range query.Group {
			if group.MappingCode != "" {
				description += "any(empty(" + query.GetTableName(group.MappingCode) + ".DESCRIPTION) ? " + group.DbColumnName + " : " + query.GetTableName(group.MappingCode) + ".DESCRIPTION),"
			} else {
				description += "any(" + group.DbColumnName + "),"
			}
		}

		if description != "" && query.SqlSelect == "" {
			description = description[0 : len(description)-1]
		}

		sqlSelect = groupBy + "," + description + query.SqlSelect
		groupBy = " GROUP BY " + groupBy

	} else {
		sqlSelect = query.SqlSelect
	}

	// Build the 'from'.
	sqlFrom = query.SqlFrom
	if sqlFrom == "" {
		sqlFrom, _ = query.GetFromTable(tableName)
	}

	// Build the sql query with the right timezone if necessary.
	return query.ReplaceTZOnQuery("SELECT "+sqlSelect+" FROM "+sqlFrom+" WHERE "+filter+groupBy+" "+query.SqlOrder+" "+query.Limit, params)
}

// This function ensure replace of '*TZ*
func (query *DMQuery) ReplaceTZOnQuery(sql string, params []interface{}) string {
	sql = strings.TrimRight(strings.ReplaceAll(sql, TIMEZONE_PATTERN, query.User.TimeZone), " ")
	log.Printf(sql+" --> params: %v", params)

	return sql
}

// This function gets the complete table name for the 'from' clause and adds the join if it's necessary. Returns the 'from' with the join and the 'from' without the join (just the main table)
// The join will be "MAIN_TABLE LEFT JOIN DIMENSION_TABLE ON ON MAIN_TABLE.GROUP_BY_DBCOLUMN = DIMENSION_TABLE.ID".
func (query *DMQuery) GetFromTable(tableName string) (string, string) {

	// If we already have set a 'tableName' on the query object, use this. Otherwise use the parameter.
	mainTable := query.GetTableName(tableName)
	sqlFrom := mainTable

	for _, group := range query.Group {
		if group.MappingCode != "" {
			dictTable := query.GetTableName(group.MappingCode)
			sqlFrom += " LEFT JOIN " + dictTable + " ON " + mainTable + "." + group.DbColumnName + " = " + dictTable + ".ID"
		}
	}

	return sqlFrom, mainTable
}

// This function returns the name of a 'ClickHouse' table for this api.
func (query *DMQuery) GetTableName(tableName string) string {
	return query.Api.Code + "_" + tableName
}

// This function returns if the api contains this property.
func (query *DMQuery) ContainsApiProperty(propertyCode string) bool {

	for _, v := range query.Api.Properties {
		if v == propertyCode {
			return true
		}
	}

	return false
}

// This function returns the 'group by' to add to the select.
func (query *DMQuery) GetGroupBy() string {

	var s bytes.Buffer

	if query.Group != nil {

		for _, g := range query.Group {
			s.WriteString(g.DbColumnName)
			s.WriteString(",")
		}

	}

	return strings.TrimSuffix(s.String(), ",")
}

// This function returns the value of a parameter
func (query *DMQuery) GetParameter(name string) string {
	if query.Params != nil {
		return query.Params[name]
	}
	return ""
}

// This function returns the value of a parameter as a 'uint64'.
func (query *DMQuery) GetParameterAsUInt64(name string) uint64 {
	if query.Params != nil {
		value, ok := query.Params[name]
		if ok {
			u64, _ := strconv.ParseUint(value, 10, 64)
			return u64
		}
	}
	return 0
}

// This function sets the chart type, from 1 to 6:
// 1. Una vista en la que no hay group by.

// 2. Un group by por 1 solo campo que no es una medida de tiempo.
// 3. Un group by por 1 solo campo que sí es una medida de tiempo.

// 4. Un group by por 2 campos en que ninguno es una medida de tiempo y el segundo componente del group by no lleva filtro.
// 5. Un group by por 2 campos en que ninguno es una medida de tiempo y el segundo componente del group by sí lleva filtro

// 6. Un group by por 2 campos en que el primero es una medida de tiempo y el segundo no

// Note: Un group by por 2 campos en que el primero no es una medida de tiempo y el segundo sí lo es no tiene sentido.
func (query *DMQuery) SetChartType() int {

	numGroups := 0
	if query.Group != nil {
		numGroups = len(query.Group)
	}

	// No group by
	if numGroups == 0 {
		query.ChartType = NO_GROUP
		return query.ChartType
	}

	// group by with one element
	if numGroups == 1 {
		if query.Group[0].Type != TIME_MEASURE {
			query.ChartType = ONE_GROUP_NOTIME
		} else {
			query.ChartType = ONE_GROUP_TIME
		}

		return query.ChartType
	}

	// group by with two elements, none of them are time measures.
	if query.Group[0].Type != TIME_MEASURE && query.Group[1].Type != TIME_MEASURE {
		query.ChartType = TWO_GROUPS_NOTIME
		return query.ChartType
	}

	// group by with two elements, where any of them is a time measure.
	if query.Group[0].Type == TIME_MEASURE && query.Group[1].Type != TIME_MEASURE {
		query.ChartType = TWO_GROUPS_FIRST_TIME
	}

	return query.ChartType
}

// This function sets the user location.
func (query *DMQuery) SetUserLocation() error {
	var err error
	query.User.Location, err = time.LoadLocation(query.User.TimeZone)
	return err
}

// This function returns if the group-by function is requested for the report
func (query *DMQuery) isAggregateFunction(fIdx int, isPartialTotal bool) bool {
	totals := query.Totals
	if isPartialTotal {
		totals = query.SubTotals
	}
	return strings.Index(totals, ARRAY_GROUP_FUNCTIONS[fIdx]) != -1
}

// This function compute the totals for 'row' and adds them to the end of 'rows'.
func (query *DMQuery) AddSubTotalReport(rows [][]interface{}, from, to int) [][]interface{} {
	return query.AddTotalsToReport(rows, from, to, true)
}

// This function compute the totals for 'row' and adds them to the end of 'rows'.
func (query *DMQuery) AddTotalReport(rows [][]interface{}, from, to int) [][]interface{} {
	return query.AddTotalsToReport(rows, from, to, false)
}

// This function compute the totals for 'row' and adds them to the end of 'rows'.
func (query *DMQuery) AddTotalsToReport(rows [][]interface{}, from, to int, isPartialTotal bool) [][]interface{} {
	totals, foundTotals := query.ComputeReportTotals(rows, from, to, isPartialTotal)
	if foundTotals {
		rows = append(rows, query.AddReportFooter(totals, isPartialTotal)...)
	}
	return rows
}

// This function compute the totals for the report 'rows'.
func (query *DMQuery) ComputeReportTotals(rows [][]interface{}, from, to int, isPartialTotal bool) ([]*DMTotals, bool) {

	var totals []*DMTotals

	if len(rows) == 0 {
		totals = []*DMTotals{{Count: 0, Sum: 0, Max: 0, Min: 0, Avg: 0}}
		return totals, false
	}

	totals = make([]*DMTotals, to-from+1)

	// Compute count,sum,max,min for each column.
	for _, row := range rows {

		pos := 0

		for idx := from; idx <= to; idx++ {

			value := misc.GetValueAsFloat32(row[idx])

			if totals[pos] == nil {
				totals[pos] = &DMTotals{Min: math.MaxFloat32}
			}

			totals[pos].Count++
			totals[pos].Sum += value

			if value > totals[pos].Max {
				totals[pos].Max = value
			}

			if value < totals[pos].Min {
				totals[pos].Min = value
			}

			pos++
		}
	}

	// Compute the average.
	for i := 0; i < len(totals); i++ {
		totals[i].Avg = totals[i].Sum / float32(totals[i].Count)
	}

	foundTotals := false
	if isPartialTotal {
		foundTotals = query.SubTotals != ""
	} else {
		foundTotals = query.Totals != ""
	}

	return totals, foundTotals
}

// This function compute totals from a 'totals' slice (this is used to calculate the final end of the report for 2-group by reports).
func (query *DMQuery) ComputeTotalFromTotals(partialTotals [][]*DMTotals) []*DMTotals {

	if len(partialTotals) == 0 {
		return nil
	}

	totals := make([]*DMTotals, len(partialTotals[0]))

	for _, arr := range partialTotals {

		for j, total := range arr {

			if totals[j] == nil {
				totals[j] = &DMTotals{Min: math.MaxFloat32}
			}

			totals[j].Count += total.Count
			totals[j].Sum += total.Sum

			if total.Max > totals[j].Max {
				totals[j].Max = total.Max
			}

			if total.Min < totals[j].Min {
				totals[j].Min = total.Min
			}
		}
	}

	// Compute the average.
	for i := 0; i < len(totals); i++ {
		totals[i].Avg = totals[i].Sum / float32(totals[i].Count)
	}

	return totals
}

// This function compute totals for all report rows and adds them to the end of the report.
func (query *DMQuery) AddReportFooter(totals []*DMTotals, isPartialTotal bool) [][]interface{} {

	if totals == nil {
		return nil
	}

	result := make([][]interface{}, 0, 2)

	for fIdx := 0; fIdx < len(ARRAY_GROUP_FUNCTIONS); fIdx++ {

		if query.isAggregateFunction(fIdx, isPartialTotal) {

			if len(query.Group) == 1 {
				result = append(result, buildTotalRow(ARRAY_GROUP_FUNCTIONS[fIdx], nil, totals, fIdx))
			} else {
				if isPartialTotal {
					result = append(result, buildTotalRow("", ARRAY_GROUP_FUNCTIONS[fIdx], totals, fIdx))
				} else {
					result = append(result, buildTotalRow(ARRAY_GROUP_FUNCTIONS[fIdx], "", totals, fIdx))
				}
			}
		}
	}

	return result
}

// This function buils the total/subtotal row.
func buildTotalRow(col1, col2 interface{}, totals []*DMTotals, fIdx int) []interface{} {

	i := 0
	n := len(totals) + 2
	if col2 == nil {
		n--
	}

	row := make([]interface{}, n)
	row[i] = col1
	i++
	if col2 != nil {
		row[i] = col2
		i++
	}

	for _, v := range totals {
		row[i] = getTotalValue(v, fIdx)
		i++
	}

	return row
}

// This function gets the 'reportKey1' and the 'partialTotals' for 'key1' sorting 'reportKey1'.
func (query *DMQuery) GetKey1Info(reportKey1 [][]interface{}, from, to int) ([][]interface{}, []*DMTotals, bool) {

	// Now sort all the elements contained within the 'key1' by the value in the last position and remove the 'key1' from all elements with position > 0.
	resultKey1 := misc.SortAndGet(reportKey1, query.Results, query.Order, len(reportKey1[0])-1)
	for k := 1; k < len(resultKey1); k++ {
		resultKey1[k][0] = ""
	}

	// Compute the totals for this 'key1' and add this totals to 'reportKey' as a last row.
	totalsKey1, foundTotals := query.ComputeReportTotals(resultKey1, from, to, true)
	if foundTotals {
		resultKey1 = append(resultKey1, query.AddReportFooter(totalsKey1, true)...)
	}

	return resultKey1, totalsKey1, foundTotals
}

// This function gets the 'reportKey1' and the 'partialTotals' for 'key1' without sorting 'reportKey1'.
func (query *DMQuery) GetKey1InfoNoSort(reportKey1 [][]interface{}, from, to int) ([][]interface{}, []*DMTotals, bool) {

	// Now sort all the elements contained within the 'key1' by the value in the last position and remove the 'key1' from all elements with position > 0.
	for k := 1; k < len(reportKey1); k++ {
		reportKey1[k][0] = ""
	}

	// Compute the totals for this 'key1' and add this totals to 'reportKey' as a last row.
	totalsKey1, foundTotals := query.ComputeReportTotals(reportKey1, from, to, true)
	if foundTotals {
		reportKey1 = append(reportKey1, query.AddReportFooter(totalsKey1, true)...)
	}

	return reportKey1, totalsKey1, foundTotals
}

// This function returns the value of the DMTotal struct depending on the field position
func getTotalValue(total *DMTotals, fIdx int) interface{} {

	switch fIdx {
	case 0:
		return total.Count
	case 1:
		return total.Sum
	case 2:
		return total.Max
	case 3:
		return total.Min
	case 4:
		return total.Avg
	}

	return nil
}

// Method that returns the query name or report name
func (query *DMQuery) GetName() string {
	if query.ExecuteReportDetails != nil {
		return query.ExecuteReportDetails.Name
	}

	return query.Name
}
