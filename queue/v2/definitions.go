package v2

import (
	newamqp "github.com/rabbitmq/amqp091-go"
	"sync"
)

const (
	// Queues
	API_QUEUE_NAME    = "DM_API_QUEUE"
	BACKUP_QUEUE_NAME = "DM_API_BACKUP_QUEUE" // Backup Queue
	REPORT_QUEUE_NAME = "DM_REPORT_QUEUE"     // REPORT Queue

	// Exchanges names and types
	API_EXCHANGE    = "API_EXCHANGE"
	BACKUP_EXCHANGE = "BACKUP_EXCHANGE"

	// Header for backup queue
	X_DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange"
	X_MESSAGE_TTL_NAME     = "x-message-ttl"
	X_MESSAGE_TTL_VALUE    = int32(20000)

	// Exchange types
	DIRECT_EXCHANGE_TYPE = "direct"
	FANOUT_EXCHANGE_TYPE = "fanout"

	kSleepTime           = 1 // sleep time before reconnecting and seconds between errors to print trace.
	kStopRabbitConnector = 1 // Time to wait for the 'RabbitConnector' to stop
)

// Queue details.
type DMQueue struct {
	Name          string              // Main queue name
	Conn          *newamqp.Connection // Connection used to connect to the queue
	ChNotifyClose chan *newamqp.Error // Channel for connection close notifications on this connection
	ChGoClose     chan bool           // Channel to used to shut down the go-routine 'RabbitConnector' in case of a 'channel closed'
	ExchangeName  string              // Exchange name to be used
	ExchangeType  string              // Exchange type to be used
	Lock          sync.Mutex          // Lock used when reconnecting
}
