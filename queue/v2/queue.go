package v2

import (
	"bitbucket.org/datametrycs-public/common-service/email"
	"bitbucket.org/datametrycs-public/common-service/http"
	"errors"
	newamqp "github.com/rabbitmq/amqp091-go"
	"log"
	"time"
)

// InitQueue : This function initializes the queue.
func InitQueue(secure bool, qUser, qPass, qAddress string, qPort string, qPath string, retry bool, args newamqp.Table, queueName, exchangeName, exchangeType string, msg *email.Message) (*DMQueue, error) {
	var err error

	// Create the queue.
	queue := DMQueue{Name: queueName}

	// If 'RABBIT_ADDRESS' is '-' then the queue is not created. This is done in order to work in non-production environments, avoiding from this system to get info from the production environment.
	if qAddress != "-" {

		if queue.Conn, err = GetRabbitConnection(secure, qUser, qPass, qAddress, qPort, qPath, retry); err != nil {
			return nil, errors.New("[Queue] The '" + queueName + "' queue could not be started because the connection could not be established. Reason: " + err.Error())
		}

		// Create the 'rabbitmq' error channels and assign them to the connections.
		queue.ChNotifyClose = make(chan *newamqp.Error)
		queue.Conn.NotifyClose(queue.ChNotifyClose)

		// Set the 'ChGoClose' to close the 'rabbitConnector' go-routine
		queue.ChGoClose = make(chan bool)

		// Declare and bind queue.
		DeclareAndBindQueue(secure, qUser, qPass, qAddress, qPort, qPath, queueName, exchangeName, exchangeType, args)

		// Run the callback in a separate thread
		go rabbitReconnector(secure, qUser, qPass, qAddress, qPort, qPath, queueName, queue.ChNotifyClose, queue.ChGoClose, msg)
	}

	return &queue, nil
}

// GetDefaultArgs : Create and return bind queues and exchanges as default args if necessary
func GetDefaultArgs() newamqp.Table {
	var args = make(newamqp.Table)
	args[X_DEAD_LETTER_EXCHANGE] = API_EXCHANGE
	args[X_MESSAGE_TTL_NAME] = X_MESSAGE_TTL_VALUE

	return args
}

// GetRabbitConnection : This functions opens the connection to the Rabbit MQ server.
func GetRabbitConnection(secure bool, qUser, qPassw, qAddress string, qPort string, qPath string, retry bool) (*newamqp.Connection, error) {
	var prefix = "amqp://"
	if secure {
		prefix = "amqps://"
	}

	for {
		conn, err := newamqp.Dial(prefix + qUser + ":" + qPassw + "@" + qAddress + ":" + qPort + "/" + qPath)
		if err == nil {
			if retry {
				log.Printf("[Queue] Connection to the api queue successfully re-established.")
			}
			return conn, nil
		}

		// Log error.
		if !retry {
			return nil, err
		}

		// Sleep 1 second before trying to reconnect ...
		log.Printf("[Queue] Sleep for %d seconds before reconnecting to the queue.", kSleepTime)
		time.Sleep(time.Duration(kSleepTime) * time.Second)
	}
}

// RabbitReconnector : This function re-establishes the connection to RabbitMQ in case the connection has died.
func rabbitReconnector(secure bool, qUser, qPassw, qAddress string, qPort string, qPath string, qName string, chNotifyCloseError chan *newamqp.Error, chGoClose chan bool, message *email.Message) {
	var err *newamqp.Error
	var s string
	stop := false

	log.Printf("[Queue] RabbitReconnector :: entering ...")

	for !stop {
		select {
		case stop = <-chGoClose:
			{
			}
		case err = <-chNotifyCloseError:
			{
				if err != nil {
					s = "Connection close received from the '" + qName + "' queue: " + err.Error() + ". Trying to re-establish the connection ...\n"
					log.Printf("[Queue] Connection close received from the '%v' queue: %v. Trying to re-establish the connection ...", qName, err)

					conn, err := GetRabbitConnection(secure, qUser, qPassw, qAddress, qPort, qPath, true)
					if err == nil {
						chNotifyCloseError = make(chan *newamqp.Error)
						conn.NotifyClose(chNotifyCloseError)

						log.Printf("[Queue] Connection to the '%v' queue successfully reestablished", qName)
						s += "Connection to the '" + qName + "' queue successfully reestablished"
					} else {
						log.Printf("[Queue] Error trying to re-establish connection to the '%v' queue: %v", qName, err)
						s += "Error trying to re-establish connection to the '" + qName + "' queue: " + err.Error()
					}

					// Send email
					if message != nil {
						message.BodyContentType = http.TEXT_PLAIN
						message.Body = s
						go email.SendEmail(message)
					}
				}
			}
		}
	}

	log.Printf("[Queue] RabbitReconnector :: exiting ...")
}

// EnqueueMessage : This functions enqueues a new message to a queue ('main' or 'backup' or 'report')
func EnqueueMessage(queue *DMQueue, message []byte, message_type string) error {
	ch, err := queue.Conn.Channel()
	if err != nil {
		return err
	}

	defer ch.Close()

	// Publish the message ...
	return ch.Publish(
		queue.ExchangeName, // exchange
		queue.Name,         // routing key
		false,              // mandatory
		false,              // immediate
		newamqp.Publishing{
			DeliveryMode: newamqp.Persistent,
			ContentType:  http.APPLICATION_JSON,
			Body:         message,
			Type:         message_type,
		})
}

// ReconnectQueueConnection : This function tries to reconnect the queue connection in case of disconnection (channel close)
func ReconnectQueueConnection(queue *DMQueue, message []byte, err error, secure bool, qUser, qPass, qAddress string, qPort string, qPath string, retry bool, args newamqp.Table, emsg *email.Message) (*DMQueue, error) {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	if err = EnqueueMessage(queue, message, ""); err != nil {
		queue.ChGoClose <- true // force to shut down the 'rabbitConnector' go-routine
		time.Sleep(time.Duration(kStopRabbitConnector) * time.Second)

		if queue, err = InitQueue(secure, qUser, qPass, qAddress, qPort, qPath, retry, args, queue.Name, queue.ExchangeName, queue.ExchangeType, emsg); err != nil {
			return queue, err
		}

		if err = EnqueueMessage(queue, message, ""); err != nil {
			queue.ChGoClose <- true // force to shut down again he 'rabbitConnector' go-routine
			return queue, err
		}
	}

	return queue, nil
}

// DeclareAndBindQueue : This function declares a queue and binds it to an exchange.
func DeclareAndBindQueue(secure bool, qUser, qPassw, qAddress, qPort string, qPath string, queueName, exchangeName, exchangeType string, args newamqp.Table) {
	var conn *newamqp.Connection
	var ch *newamqp.Channel
	var err error

	// Get a connection.
	if conn, err = GetRabbitConnection(secure, qUser, qPassw, qAddress, qPort, qPath, false); err != nil {
		log.Panicf("An unexpected error has occurred trying to get a new connection to create and bind queues and exchanges: %v", err)
	}
	defer conn.Close()

	// Get a channel from this connection ...
	if ch, err = conn.Channel(); err != nil {
		log.Panicf("[Queue] An unexpected error has occurred trying to open a new channel to create and bind queues and exchanges: %v", err)
	}
	defer ch.Close()

	// Declare the 'exchange' if it exists
	if exchangeName != "" {
		if err = ch.ExchangeDeclare(
			exchangeName, // name
			exchangeType, // type
			true,         // durable
			false,        // auto-deleted
			false,        // internal
			false,        // no-wait
			nil,          // arguments
		); err != nil {
			log.Panicf("[Queue] An unexpected error has occurred trying to declare the '%s' exchange: %v", exchangeName, err)
		}
	}

	// Declare and bind queue to the exchange IF AND ONLY IF the exchange type is not 'fanout'.
	if exchangeType != FANOUT_EXCHANGE_TYPE {
		if _, err = ch.QueueDeclare(
			queueName, // name
			true,      // durable
			false,     // delete when unused
			false,     // exclusive
			false,     // no-wait
			args,      // arguments
		); err != nil {
			log.Panicf("[Queue] An unexpected error has occurred trying to declare the '%s' queue: %v", queueName, err)
		}

		// Bind the worker exchange (if we have exchange ...)
		if exchangeName != "" {
			if err = ch.QueueBind(queueName, "", exchangeName, false, nil); err != nil {
				log.Panicf("[Queue] An unexpected error has occurred trying to bind the '%s' queue to the '%s' exchange: %v", queueName, exchangeName, err)
			}
		}
	}
}
