package http

import (
	"bitbucket.org/datametrycs-public/common-service/gzip"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	HTTP_PROTOCOL = "http://"

	// Headers
	CONTENT_TYPE_HEADER     = "Content-Type"
	CONTENT_ENCODING_HEADER = "Content-Encoding"
	CONTENT_LENGTH_HEADER   = "Content-Length"
	AUTHORIZATION_HEADER    = "Authorization"
	KEEP_ALIVE_HEADER       = "Keep-Alive"
	CONNECTION_HEADER       = "Connection"

	// Content-Type
	APPLICATION_FORM_URL_ENCODED = "application/x-www-form-urlencoded"
	APPLICATION_JSON             = "application/json"
	TEXT_PLAIN                   = "text/plain"

	// Health check response
	HTTP_HEALTHCHECK_MSG = "OK"

	// Others
	GZIP_COMPRESSION = "gzip"
	SIZE_64K         = 64 * 1024
)

// This function initializes the http client. The 'endpoint' address is 'protocol://host.port' (i.e. https://datametrycs.grupoventus.com:8443)
func CreateHttpClient(connectTimeout int, responseTimeout int) http.Client {

	// Create the transport and then the http client instances that will be shared among all go routines.
	tr := &http.Transport{
		DisableKeepAlives:  true,
		DisableCompression: true,
		Dial: (&net.Dialer{
			Timeout:   time.Duration(connectTimeout) * time.Millisecond, // Dial.Dialer.Timeout is the maximum amount of time a dial will wait for a connect to complete.
			KeepAlive: 0,
		}).Dial,
	}

	// Create the 'http' client.
	return http.Client{
		Transport: tr,
		Timeout:   time.Duration(connectTimeout+responseTimeout) * time.Millisecond, // specifies a time limit for requests made by this Client. The timeout includes connection time and reading the response body.
	}
}

// This function sends an 'APPLICATION_FORM_URL_ENCODED' post request.
func SendFormUrlEncodedRequest(objHttp http.Client, method string, url string, params string) ([]byte, int, error) {

	// Send request
	var req *http.Request
	var resp *http.Response
	var body []byte
	var err error

	// Create and send request.
	if method == http.MethodGet || method == http.MethodHead {
		req, err = http.NewRequest(method, url+"?"+params, nil)
	} else {
		req, err = http.NewRequest(method, url, bytes.NewReader([]byte(params)))
	}

	req.Header.Set("Content-Type", APPLICATION_FORM_URL_ENCODED)
	resp, err = objHttp.Do(req)

	if err != nil {
		return nil, http.StatusGatewayTimeout, err
	}
	if resp == nil {
		return nil, http.StatusBadGateway, errors.New("The response body is empty.")
	}

	// Defer closing the response body.
	defer resp.Body.Close()

	// Force to read the response body.
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	// If there is a 'content-encoding' --> decompress 'gzip'.
	if resp.Header.Get(CONTENT_ENCODING_HEADER) == GZIP_COMPRESSION {
		if body, err = gzip.Decompress(body); err != nil {
			return nil, 0, err
		}
	}

	return body, resp.StatusCode, err
}

// This function sends a json post request to the query service.
func SendPostRequest(objHttp http.Client, url, contentType string, request []byte) ([]byte, error) {

	var req *http.Request
	var resp *http.Response
	var body []byte
	var err error

	// Create and send request.
	req, err = http.NewRequest(http.MethodPost, url, bytes.NewReader(request))
	req.Header.Set("Content-Type", contentType)
	resp, err = objHttp.Do(req)
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.New("The response body is empty.")
	}

	// Defer closing the response body.
	defer resp.Body.Close()

	// Force to read the response body.
	if body, err = ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	}

	return body, nil
}

// This function reads a 'JSON' request.
func ReadJsonRequest(r *http.Request) ([]byte, int, error) {

	// 'content-type' must contain 'application/json'
	ct := strings.ToLower(r.Header.Get(CONTENT_TYPE_HEADER))
	if strings.Index(ct, APPLICATION_JSON) == -1 {
		return nil, http.StatusBadRequest, errors.New("A '" + CONTENT_TYPE_HEADER + "' header with '" + APPLICATION_JSON + "' value is required.")
	}

	// Read the request body.
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, errors.New("An unexpected error has occurred trying to read the request body: " + err.Error())
	}

	// If empty body, error.
	if body == nil || len(body) == 0 {
		return nil, http.StatusBadRequest, errors.New("The json message is empty.")
	}

	// Return.
	return body, http.StatusOK, nil
}

// This function returns a 200 on the http response.
func TraceListenerSuccess(w http.ResponseWriter, msg string) {

	// Marshall the 'msg' to a []byte and build the json message to return.
	str, _ := json.Marshal(msg)
	strJson := "{ \"message\": " + string(str) + " }"

	// Write the json to the http response.
	WriteHttpResponse(w, APPLICATION_JSON, []byte(strJson), http.StatusOK)
}

// This function traces an error and prints it to the http response.
func TraceListenerError(w http.ResponseWriter, msg string, err error, statusCode int) {

	if err != nil {
		msg = fmt.Sprintf(msg, err)
	}

	// Marshall the 'msg' to a []byte and build the json message to return.
	str, _ := json.Marshal(msg)
	strJson := "{ \"error\": " + string(str) + " }"

	// Write the json to the http response.
	WriteHttpResponse(w, APPLICATION_JSON, []byte(strJson), statusCode)
}

// This function returns the http response.
func WriteHttpResponse(w http.ResponseWriter, contentType string, msg []byte, statusCode int) (int, error) {

	// First we write the 'content-type' and 'content-length' headers and 'statusCode'.
	w.Header().Set(CONTENT_TYPE_HEADER, contentType)
	w.Header().Set(CONTENT_LENGTH_HEADER, strconv.Itoa(len(msg)))
	w.WriteHeader(statusCode)

	// And finally write the response body.
	return w.Write(msg)
}

// This functions return if the error is a connection timeout error.
func IsConnectTimeout(err error) bool {
	if err == nil {
		return false
	}
	return strings.Index(err.Error(), "i/o timeout") != -1
}

// This functions return if the error is a response timeout error.
func IsResponseTimeout(err error) bool {
	if err == nil {
		return false
	}
	if strings.Index(err.Error(), "Client.Timeout exceeded") != -1 {
		return true
	}
	return strings.Index(err.Error(), "context deadline exceeded") != -1
}

// The 'health check' handler
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) (int, error) {
	return WriteHttpResponse(w, TEXT_PLAIN, []byte(HTTP_HEALTHCHECK_MSG), http.StatusOK)
}
