package gzip

import (
	"bytes"
	"compress/gzip"
	"io/ioutil"
)

// This function performs a gzip compression, returns nil if there was any error compressing.
func Compress(body []byte) ([]byte, error) {

	var b bytes.Buffer

	w := gzip.NewWriter(&b)
	_, err := w.Write(body) // 'body' carries the decompressed json
	if err != nil {
		return nil, err
	}

	// Before returning the compressed byte array, the 'w' must be closed.
	if err = w.Close(); err != nil {
		return nil, err
	}

	// Return compressec content.
	return b.Bytes(), nil
}

// This function performs a gzip decompression, returns nil if there was any error decompressing.
func Decompress(body []byte) ([]byte, error) {

	r, err := gzip.NewReader(bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	defer r.Close()

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return data, nil
}
