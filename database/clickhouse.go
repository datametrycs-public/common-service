package database

import (
	"bytes"
	"database/sql"
	_ "github.com/ClickHouse/clickhouse-go"
	"strconv"
	"strings"
	"sync"
)

const (
	DB_CLICKHOUSE_TCP_PROTOCOL        = "tcp://"
	DB_CLICKHOUSE_DRIVER              = "clickhouse"
	DB_CLICKHOUSE_PORT                = "9440"
	DB_CLICKHOUSE_CREATOR_DB_PREFIX   = "DM_"
	DB_CLICKHOUSE_CREATOR_USR_PREFIX  = "USR_"
	DB_CLICKHOUSE_CREATOR_ROLE_PREFIX = "ROLE_"
	DB_CLICKHOUSE_READ_TIMEOUT        = 90
	DB_CLICKHOUSE_WRITE_TIMEOUT       = 10
	ERR_BAD_CONNECTION                = "bad connection"
	ERR_TIMEOUT_CONNECTION            = "timeout"
)

var (
	connections = new(sync.Map) // This 'map' contains the connections corresponding to each 'dbAddress'. So, 'key' = 'companyDbAddress + "|" + companyCode + "|" + companyPassword', 'value' = *sql.DB.
	dbMutex     sync.Mutex
)

// This function returns the 'clickhouse' database corresponding to this 'companyDbAddress'.
func GetDatabase(companyDbAddress, companyCode, companyPassword string, isRoot bool) (*sql.DB, error) {
	return GetDatabaseWithTimeouts(companyDbAddress, companyCode, companyPassword, DB_CLICKHOUSE_READ_TIMEOUT, DB_CLICKHOUSE_WRITE_TIMEOUT, isRoot)
}

// This function returns the 'clickhouse' database corresponding to this 'companyDbAddress'.
func GetDatabaseWithTimeouts(companyDbAddress, companyCode, companyPassword string, readTimeout, writeTimeout int, isRoot bool) (*sql.DB, error) {
	key := getConnectionKey(companyDbAddress, companyCode, companyPassword)

	// If the database already exists for this 'companyCode', it's returned.
	if idb, ok := connections.Load(key); ok {
		return idb.(*sql.DB), nil
	}

	// Lock on the global database creation mutex.
	dbMutex.Lock()
	defer dbMutex.Unlock()

	// Check database existence again.
	if idb, ok := connections.Load(key); ok {
		return idb.(*sql.DB), nil
	}

	// Open database.
	db, err := GetClickHouseDatabase(companyDbAddress, companyCode, companyPassword, readTimeout, writeTimeout, isRoot)
	if err != nil {
		return nil, err
	}

	// Store the database for this company.
	connections.Store(key, db)
	return db, nil
}

// This function returns the key to 'connections
func getConnectionKey(companyDbAddress, companyCode, companyPassword string) string {
	return companyDbAddress + "|" + companyCode + "|" + companyPassword
}

// This function removes the 'clickhouse' database corresponding to this 'companyCode' when there is a 'Bad connection' error
func RemoveDatabaseOnBadConnectionError(companyDbAddress, companyCode, companyPassword string, err error) bool {
	if strings.Contains(err.Error(), ERR_BAD_CONNECTION) {
		connections.Delete(getConnectionKey(companyDbAddress, companyCode, companyPassword))
		return true
	}

	if strings.Contains(err.Error(), ERR_TIMEOUT_CONNECTION) {
		return true
	}

	return false
}

// This function returns the clickhouse database and performs a ping
func GetClickHouseDatabase(companyDbAddress, companyCode, companyPassword string, readTimeout, writeTimeout int, isRoot bool) (*sql.DB, error) {
	username := companyCode
	password := companyPassword
	database := ""

	if !isRoot {
		username = GetDatabaseUser(companyCode)
		database = "&database=" + GetDatabaseName(companyCode)
	}

	st := DB_CLICKHOUSE_TCP_PROTOCOL + companyDbAddress + ":" + DB_CLICKHOUSE_PORT + "?secure=true&username=" + username + "&password=" + password + database +
		"&read_timeout=" + strconv.Itoa(readTimeout) + "&write_timeout=" + strconv.Itoa(writeTimeout)

	// Try to get a handle to the database.
	db, err := sql.Open(DB_CLICKHOUSE_DRIVER, st)
	if err != nil {
		return nil, err
	}

	// Try to ping if database is alive.
	if err = db.Ping(); err != nil {
		return nil, err
	}

	// Pool ok.
	return db, nil

}

// This function returns the name of the 'ClickHouse' database for this company
func GetDatabaseName(companyCode string) string { return DB_CLICKHOUSE_CREATOR_DB_PREFIX + companyCode }

// This function returns the name of the 'ClickHouse' user for this company
func GetDatabaseUser(companyCode string) string {
	return DB_CLICKHOUSE_CREATOR_USR_PREFIX + companyCode
}

// This function returns the name of the 'ClickHouse' role for this company
func GetDatabaseRole(companyCode string) string {
	return DB_CLICKHOUSE_CREATOR_ROLE_PREFIX + companyCode
}

// This function returns the name of a 'ClickHouse' table for this company and api.
func GetTableName(companyCode string, apiCode string, tableName string) string {
	return GetDatabaseName(companyCode) + "." + apiCode + "_" + tableName
}

// This function gets the details for a 'query id' (it can contain several queries)
func GetQueryInfo(db *sql.DB, queryId string) ([][]interface{}, error) {

	// Search the query ...
	rows, err := db.Query("SELECT query_id, read_rows, total_rows_approx, elapsed "+
		"FROM clusterAllReplicas(default, system.processes) "+
		"WHERE query LIKE ? AND total_rows_approx > 0", getQueryIdField(queryId))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([][]interface{}, 0, 10)
	var readRows, totalRows uint64
	var elapsed float64

	for rows.Next() {
		if err = rows.Scan(&queryId, &readRows, &totalRows, &elapsed); err != nil {
			return nil, err
		}
		result = append(result, []interface{}{queryId, readRows, totalRows, elapsed, ""})
	}

	return result, nil
}

// This function kill a query with 'queryId' identifier.
func KillQuery(db *sql.DB, companyCode, queryId string) error {
	var chQueryId string

	// Perform the select to get the real 'query id'.
	rows, err := db.Query("SELECT query_id FROM clusterAllReplicas(default, system.processes) WHERE query LIKE ?", getQueryIdField(queryId))
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		if err = rows.Scan(&chQueryId); err != nil {
			return err
		}
		if _, err = db.Exec("KILL QUERY ON CLUSTER "+GetDatabaseName(companyCode)+" WHERE query_id = ? ASYNC", chQueryId); err != nil {
			return err
		}
	}

	return nil
}

// This function generate the string '%queryId=queryId%". It's a copy of the same method on 'QueryService.utils.queryUtils'.
func getQueryIdField(queryId string) string {
	var s bytes.Buffer

	s.WriteString("%")
	s.WriteString(queryId)
	s.WriteString("=")
	s.WriteString(queryId)
	s.WriteString("%")

	return s.String()
}
