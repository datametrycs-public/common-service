package database

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

const (
	DB_MARIADB_DRIVER         = "mysql"
	DB_MARIADB_PORT           = "3306"
	DB_MARIADB_DBNAME         = "dm_administrator_data"
	DB_MARIADB_MAX_IDLE_CONN  = 5
	DB_MARIADB_CONN_KEEPALIVE = 5
)

// This function validates the MariaDB connection.
func GetConnection(host, user, passwd string) (*sql.DB, error) {

	// Connect to the database. The url is like this: "user:password@tcp(ip:port)/dbname?'.
	dbConn, err := sql.Open(DB_MARIADB_DRIVER, user+":"+passwd+"@tcp("+host+":"+DB_MARIADB_PORT+")/"+DB_MARIADB_DBNAME)
	if err != nil {
		return nil, err
	}

	dbConn.SetMaxIdleConns(DB_MARIADB_MAX_IDLE_CONN)
	dbConn.SetConnMaxLifetime(DB_MARIADB_CONN_KEEPALIVE * time.Second)

	return dbConn, nil
}
